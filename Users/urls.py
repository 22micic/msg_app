

from django.conf.urls import include

from django.conf import settings
from django.urls import path

from .views import MsgUserListView, GroupList, MsgUserRegistrationView, MsgUserDetailView

urlpatterns = [
    path('users/', MsgUserListView.as_view()),
    path('users/<pk>/', MsgUserDetailView.as_view()),

    path('users/groups/', GroupList.as_view()),
    path('users/groups/<pk>/', GroupList.as_view()),

    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),

    path('users/registration/', MsgUserRegistrationView.as_view()),
]
