from django.db.models import CharField, Count
from django_filters import rest_framework as filters
from rest_framework import serializers

from Users.models import Profile

#
# ---------------------------------------------------


# class ProfileFilter(filters.FilterSet):
#     gender = filters.CharFilter()
#
#     class Meta:
#         model = Profile
#         fields = ['gender', ]
#
#     @staticmethod
#     def gender_filter(queryset, gender):
#         queryset = Profile.objects.filter(gender__in=gender).values('gender').annotate(Count('gender'))
#         return queryset
