from django.urls import path
from .views import UserStatisticsGenderView, UserStatisticsDesignationView

urlpatterns = [
    path('statistics/gender-report/', UserStatisticsGenderView.as_view()),
    path('statistics/designation-report/', UserStatisticsDesignationView.as_view()),
]
