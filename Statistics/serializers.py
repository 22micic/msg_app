from Users.models import User, Profile, Gender
from rest_framework.serializers import ModelSerializer
from rest_framework import serializers

from Users.serializers import ProfileSerializer


class UserStatisticsSerializer(ModelSerializer):
    gender = serializers.CharField()
    gender__count = serializers.IntegerField()

    class Meta:
        model = Profile
        fields = ['gender', 'gender__count']


class ProfileStatisticsFilterSerializer(ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'first_name',
                  'last_name', 'email', 'is_superuser', 'profile', ]


class GenderField(serializers.RelatedField):

    def to_representation(self, obj):
        return {
            'gender_id': obj.id,
            'gender_description': obj.gender_description,
        }

    def to_internal_value(self, data):
        try:
            try:
                obj_id = data['id']
                return Gender.objects.get(id=obj_id)
            except KeyError:
                raise serializers.ValidationError(
                    'id is a required field.'
                )
            except ValueError:
                raise serializers.ValidationError(
                    'id must be an integer.'
                )
        except Gender.DoesNotExist:
            raise serializers.ValidationError(
                'Obj does not exist.'
            )


class BarSerializer(ModelSerializer):
    gender = GenderField(
        queryset=Gender.objects.all()
    )

    class Meta:
        model = Profile
        fields = ['designation', 'bio', 'city', 'birth_date', 'gender']

    def to_representation(self, obj):
        """Move fields from profile to user representation."""
        representation = super().to_representation(obj)
        representation['city'] = representation['city'].lower()
        gender_representation = representation.pop('gender')
        for key in gender_representation:
            representation[key] = gender_representation[key]

        return representation
