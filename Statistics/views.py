from _mysql import connection

from django.db.models import Count
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import filters, serializers
from Users.models import User, Profile
from .serializers import UserStatisticsSerializer, ProfileStatisticsFilterSerializer, BarSerializer


class UserStatisticsGenderView(ListAPIView):
    """
    A class that uses a '.get_queryset(self):' method
    to count the number of user with the same gender.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = UserStatisticsSerializer
    queryset = Profile.objects.all()
    search_fields = ('gender', )

    def get_queryset(self):
        gender = self.request.query_params.get('gender')
        print(self.request.query_params)
        queryset = Profile.objects.filter(gender=gender).values('gender').annotate(Count('gender'))
        return queryset


class UserStatisticsDesignationView(ListAPIView):
    """
    Filter by designation.
    Can also add other fields to filter by.
    """
    permission_classes = [AllowAny]
    serializer_class = ProfileStatisticsFilterSerializer
    queryset = User.objects.all()
    filter_backends = (filters.SearchFilter,)
    search_fields = ['profile__designation', ]

